# Travail Pratique (Cours de Java pro, ZZ3)

L'objectif de ce TP est de créer une bibliothèque en Java permettant de faire de l'injection de dépendances.

## Dépendances

Pour mon travail j'ai décidé d'utiliser 2 dépendances:
- JUnit : pour les tests unitaires
- Javax.Json : pour lire les valeurs à injecter depuis un fichier de configuration

## Fonctionnalités

### Conteneur de dépendances

La bibliothèque permet tout d'abord la gestion des implémentations. On peut ainsi enregistrer une classe à utiliser à la place d'une interface (ou bien d'une autre classe).

```java
/*
 * La classe Item sera utilisée pour instancier les variables du type IItem.
 */
DependancyContainer.register(IItem.class, Item.class);
```

On peut ensuite gérer l'instanciation via le conteneur de dépendances.

```java
/*
 * Une instance du Item sera créée.
 */
DependancyContainer.newInstance(IItem.class);
```

Il est impossible d'enregistrer des dépendances nulles (contrat ou implémentation qui vaut `null`). Dans ce cas la fonction d'enregistrement ne fait rien. Si aucun lien d'héritage n'existe entre le contrat et l'implémentation, alors une exception `NoImplementationException` est levée.

Il est possible d'indiquer des dépendances directement dans le fichier `src/main/resources/dependancies.properties`.
Il ne faut alors que charger les informations de ce fichier grâce au conteneur de dépendances.

```java
DependancyContainer.loadValues();
```

### Injection de dépendances

L'injection des dépendances est facilité par des annotations. Trois types d'injections sont possibles.

| Type d'injection | Annotation à utiliser |
| ------ | ------ |
| Constructeur | `InjectByConstructor` |
| Attributs | `InjectByField` |
| Setters | `InjectBySetter` |

Dans chacun de ces cas, le conteneur de dépendances rechercher quelle classe il doit réellement instancié. Puis il regarde quelles injections il doit y faire via les annotations.

### Conteneur de valeurs

Il est possible de gérer des valeurs via un conteneur. Pour cela il faut utiliser le fichier *src/main/resources/config.json*. Il est possible d'obtenir directement des booléens, des nombres flottants (double), des chaînes de caractères et des `null`. Les formats complexes telles que les tableaux ou les objets sont également accessibles mais ils sont renvoyés comme étant respectivement des `javax.json.JsonArray`et des ``javax.json.JsonObject`.

Les nombres peuvent être automatiquement convertis dans d'autres types Java. Pour cela, il faut utiliser un suffixe sur le nom de la clé :

| Type | Suffixe |
| ------ | ------ |
| Integer | `_i` |
| Long | `_l` |
| BigInteger | `_bi` |
| Double | `_d` |
| BigDecimal | `_bd` |

Il est aussi possible de forcer l'insertion directement depuis le code java.

```java
ValueContainer.addValue("maCle", new MaClasse());
```

Pour accéder à une valeur on peut passer directement par le conteneur.

```java
ValueContainer.getValue("maCle");
```

### Injection de valeurs

Pour simplifier l'utilisation du conteneur de valeur, il est possible d'injecter directement des valeurs dans les attributs d'une instance. Il suffit d'annoter les attributs à injecter avec `` et d'appeler l'injection.

```java
public class Item {
    @InjectValue("maCle") public Double d;
}

Item item = new Item();
ValueInjector.injectValues(item);
``` 
