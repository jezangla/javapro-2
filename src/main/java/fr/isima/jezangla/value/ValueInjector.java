package fr.isima.jezangla.value;

import java.lang.reflect.Field;

public class ValueInjector {

	public static void injectValues(Object obj) {
		if (null == obj) return;
		
		for (Field f : obj.getClass().getFields()) {
			if (f.isAnnotationPresent(InjectValue.class)) {
				try {
					f.set(obj, ValueContainer.getValue(f.getDeclaredAnnotation(InjectValue.class).value()));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
