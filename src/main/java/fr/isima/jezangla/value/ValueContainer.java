package fr.isima.jezangla.value;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonString;

public class ValueContainer {

	private static HashMap<String, Object> values = new HashMap<>();

	/*
	 * Charge les valeurs d'un fichier de configuration dans le conteneur de valeur.
	 */
	public static void loadValues() {
		try {
			File f = new File("src/main/resources/config.json");
			JsonReader parser = Json.createReader(new FileReader(f));
			JsonObject root = parser.readObject();
			parser.close();

			root.forEach((s, v) -> {
				switch (v.getValueType()) {
				case STRING:
					values.put(s, ((JsonString) v).getString());
					break;
				case OBJECT:
					values.put(s, (JsonObject) v);
					break;
				case ARRAY:
					values.put(s, ((JsonArray) v).toArray());
					break;
				case FALSE:
					values.put(s, false);
					break;
				case TRUE:
					values.put(s, true);
					break;
				case NULL:
					values.put(s, null);
					break;
				case NUMBER:
					JsonNumber n = (JsonNumber) v;
					if (s.endsWith("_d")) { // Force un Double
						values.put(s, (Double) n.doubleValue());
					} else if (s.endsWith("_bd")) { // Force un BigDecimal
						values.put(s, (BigDecimal) n.bigDecimalValue());
					} else if (s.endsWith("_i")) { // Force un Integer
						values.put(s, (Integer) n.intValue());
					} else if (s.endsWith("_bi")) { // Force un BigInteger
						values.put(s, (BigInteger) n.bigIntegerValue());
					} else if (s.endsWith("_l")) { // Force un Long
						values.put(s, (Long) n.longValue());
					} else { // Si rien n'est sp�cifi� on charge un Double
						values.put(s, (Double) n.doubleValue());
					}
					break;
				}
			});

		} catch (FileNotFoundException e) {
			System.err.println("Config file not found.");
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked") // Le warning ne nous est pas util, en effet si il y a une erreur de type, le
									// programme ne doit pas fonctionner (pas besoin de s'assurer de la bonne
									// gestion)
	public static <T> T getValue(String key) {
		return (T) values.getOrDefault(key, null);
	}
	
	public static void addValue(String key, Object value) {
		if (values.containsKey(key)) return;
		values.put(key, value);
	}
}
