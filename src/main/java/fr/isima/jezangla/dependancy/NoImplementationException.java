package fr.isima.jezangla.dependancy;

public class NoImplementationException extends Exception {
	private static final long serialVersionUID = -9189977433929561066L;
	private Class<?> contract, implementation;
	
	public NoImplementationException(Class<?> contract, Class<?> implementation) {
		super();
		this.contract = contract;
		this.implementation = implementation;
	}

	@Override
	public String getMessage() {
		return "Class [" + implementation.getName() + "] can't be used to implement " + contract.getName() +".";
	}
}
