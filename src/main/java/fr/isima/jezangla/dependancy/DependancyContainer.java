package fr.isima.jezangla.dependancy;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Properties;


public class DependancyContainer {

	private static HashMap<Class<?>, Class<?>> dependancies = new HashMap<>();

	/*
	 * Charge les valeurs d'un fichier de configuration dans le conteneur de d�pendances.
	 */
	public static void loadValues() {
		try {
			File f = new File("src/main/resources/dependancies.properties");
			Properties prop = new Properties();
			prop.load(new FileReader(f));

			prop.forEach((contract, implementation) -> {
				try {
					Class<?> c = Class.forName((String) contract);
					Class<?> i = Class.forName((String) implementation);
					register(c, i);
				} catch (ClassNotFoundException | NoImplementationException e) {
					e.printStackTrace();
				}
			});
			
		} catch (IOException e) {
			System.err.println("Config file not found.");
			e.printStackTrace();
		}
	}
	
	public static void register(Class<?> contract, Class<?> implementation) throws NoImplementationException {
		if (contract == null || implementation == null) return;		
		
		if (Arrays.stream(implementation.getInterfaces()).anyMatch(inter -> {
			return inter == contract;
		})) {
			dependancies.put(contract, implementation); // Insertion seulement si il y a une notion d'h�ritage.
		} else {
			throw new NoImplementationException(contract, implementation);
		}
	}

	public static Object newInstance(Class<?> contract) {
		try {
			return getClassForContract(contract).getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Class<?> getClassForContract(Class<?> contract) {
		return dependancies.getOrDefault(contract, (contract.isInterface() ? null : contract));
	}
}
