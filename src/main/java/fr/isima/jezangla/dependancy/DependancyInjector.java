package fr.isima.jezangla.dependancy;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

import fr.isima.jezangla.dependancy.annotations.InjectByConstructor;
import fr.isima.jezangla.dependancy.annotations.InjectByField;
import fr.isima.jezangla.dependancy.annotations.InjectBySetter;

public class DependancyInjector {

	@SuppressWarnings("unchecked") // Permet d'ignorer le warning lors de la convertion juste avant le return
	public static <T> T injectByConstructor(Class<T> c) {
		if(null == c) return null; // Si aucune classe n'est demand�e, on ne renvoie rien.
		for (Constructor<?> co: DependancyContainer.getClassForContract(c).getConstructors()) { // Recherche d'un constructeur injectable ou n'ayant aucun param�tre
			if (co.isAnnotationPresent(InjectByConstructor.class) || co.getParameterCount() == 0)
			{
				Class<?>[] paramsTypes = co.getParameterTypes();
				Object[] paramsValue = Arrays.stream(paramsTypes).map(paramType -> injectByConstructor(DependancyContainer.getClassForContract(paramType))).toArray(); // Instanciation des param�tres du constructeur en passant par l'injection de constructeur.

				try {
					return (T) co.newInstance(paramsValue); // Appel du constructeur, si il y a une erreur on renvoie null.
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		
		// Aucun constructeur ne rempli les conditions pour une instanciation
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T injectBySetter(Class<T> c) {
		if(null == c) return null; // Si aucune classe n'est demand�e, on ne renvoie rien.
		Object obj;
		try {
			obj = DependancyContainer.getClassForContract(c).getConstructor().newInstance(); // Cr�ation de l'instance sur laquelle injecter les valeurs. 
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			return null;
		}
		
		// Le constructeur par d�faut fonctionne.
		
		for (Method m: DependancyContainer.getClassForContract(c).getMethods()) { // Parcours de toutes les setters r�f�renc�s pour l'injection
			if (m.isAnnotationPresent(InjectBySetter.class)) {
				// Instanciation des param�tres du constructeur en passant par l'injection de constructeur.
				Object[] paramsValue = Arrays.stream(m.getParameterTypes()).map(paramType -> injectBySetter(DependancyContainer.getClassForContract(paramType))).toArray(); 
				try {
					m.invoke(obj, paramsValue);
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			}
		}
		
		// Aucun constructeur ne rempli les conditions pour une instanciation
		return (T) obj;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T injectByField(Class<T> c) {
		if(null == c) return null; // Si aucune classe n'est demand�e, on ne renvoie rien.
		Object obj;
		try {
			obj = DependancyContainer.getClassForContract(c).getConstructor().newInstance(); // Cr�ation de l'instance sur laquelle injecter les valeurs. 
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			return null;
		}
		
		// Le constructeur par d�faut fonctionne.
		
		for (Field m: DependancyContainer.getClassForContract(c).getFields()) { // Parcours de toutes les setters r�f�renc�s pour l'injection
			if (m.isAnnotationPresent(InjectByField.class)) {
				// Instanciation des param�tres du constructeur en passant par l'injection de constructeur.
				
				try {
					m.set(obj, injectByField(DependancyContainer.getClassForContract(m.getType())));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		
		// Aucun constructeur ne rempli les conditions pour une instanciation
		return (T) obj;
	}
}
