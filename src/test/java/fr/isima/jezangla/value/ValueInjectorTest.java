package fr.isima.jezangla.value;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ValueInjectorTest {

	@BeforeEach
	void init() {
		ValueContainer.loadValues();
	}
	
	@Test
	void testValueInjection() {
		Item item = new Item();
		ValueInjector.injectValues(item);

		assertEquals((Double) 2.4, item.d);
	}
	
	@Test
	void testSeveralValuesInjections() {
		SeveralItem item = new SeveralItem();
		ValueInjector.injectValues(item);

		assertEquals((Double) 2.4, item.d);
	}
	
	@Test
	void testExtendedValuesInjections() {
		ExtendedItem item = new ExtendedItem();
		ValueInjector.injectValues(item);

		assertEquals((Double) 2.4, item.d);
		assertEquals((Integer) 3, item.i);
	}

}
