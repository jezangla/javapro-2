package fr.isima.jezangla.value;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.json.JsonNumber;
import javax.json.JsonString;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ValueContainerTest {

	@BeforeEach
	void init() {
		ValueContainer.loadValues();
	}

	@Test
	void getLong() {
		assertEquals((Double) 13., ValueContainer.getValue("keyLong"));
	}

	@Test
	void getDouble() {
		assertEquals((Double) 2.4, ValueContainer.getValue("keyDouble"));
	}

	@Test
	void getString() {
		assertEquals("Salut a tous les amis c'est David Lafarge Pokemon et Miss Jirachi ! Coucouuuuuu",
				ValueContainer.getValue("keyString"));
	}

	@Test
	void getLongArray() {
		Long[] expected = { 12L, 16L, 15L };
		Object[] actual = ValueContainer.getValue("keyLongArray");
		for (int i = 0; i < expected.length; ++i) {
			assertEquals(expected[i], (Long) ((JsonNumber) actual[i]).longValue());
		}
	}

	@Test
	void getDoubleArray() {
		Double[] expected = { 1.5, 2.5, 3.5 };
		Object[] actual = ValueContainer.getValue("keyDoubleArray");
		for (int i = 0; i < expected.length; ++i) {
			assertEquals(expected[i], (Double) ((JsonNumber) actual[i]).doubleValue());
		}
	}

	@Test
	void getStringArray() {
		String[] expected = { "Aujourd'hui", "ouverture", "de", "booster", "de", "cartes", "pokemon", "!" };
		Object[] actual = ValueContainer.getValue("keyStringArray");
		for (int i = 0; i < expected.length; ++i) {
			assertEquals(expected[i], ((JsonString) actual[i]).getString());
		}
	}

	@Test
	void testForcedNumberTypes() {
		assertEquals(Long.class.getName(), ValueContainer.getValue("keyForced_l").getClass().getName());
		assertEquals(Double.class.getName(), ValueContainer.getValue("keyForced_d").getClass().getName());
		assertEquals(BigDecimal.class.getName(), ValueContainer.getValue("keyForced_bd").getClass().getName());
		assertEquals(Integer.class.getName(), ValueContainer.getValue("keyForced_i").getClass().getName());
		assertEquals(BigInteger.class.getName(), ValueContainer.getValue("keyForced_bi").getClass().getName());
	}
}