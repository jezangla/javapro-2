package fr.isima.jezangla.dependancy;

import fr.isima.jezangla.dependancy.annotations.InjectByConstructor;

public class NeedItemByConstructorExtended extends NeedItemByConstructor{
	
	@InjectByConstructor
	public NeedItemByConstructorExtended(IItem item) {
		super(item);
	}
}