package fr.isima.jezangla.dependancy;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DependancyInjectorTest {
	
	@BeforeEach
	public void init() throws NoImplementationException {
		DependancyContainer.register(IItem.class, Item.class);
	}
	
	@Test
	public void injectInterface() {
		IItem itemConstructor = DependancyInjector.injectByConstructor(IItem.class);
		IItem itemField = DependancyInjector.injectByField(IItem.class);
		IItem itemSetter = DependancyInjector.injectBySetter(IItem.class);
		

		assertNotNull(itemConstructor);
		assertEquals('Y', itemConstructor.getVal());
		assertNotNull(itemField);
		assertEquals('Y', itemField.getVal());
		assertNotNull(itemSetter);
		assertEquals('Y', itemSetter.getVal());
	}
	
	@Test
	public void testInjectionByConstructor() {
		NeedItemByConstructor needItem = DependancyInjector.injectByConstructor(NeedItemByConstructor.class);
		
		assertNotNull(needItem);
		assertNotNull(needItem.item);
	}
	
	@Test
	public void testInjectionBySetter() {
		NeedItemBySetter needItem = DependancyInjector.injectBySetter(NeedItemBySetter.class);
		
		assertNotNull(needItem);
		assertNotNull(needItem.item);
	}
	
	@Test
	public void testInjectionByField() {
		NeedItemByField needItem = DependancyInjector.injectByField(NeedItemByField.class);
		
		assertNotNull(needItem);
		assertNotNull(needItem.item);
	}
	
	@Test
	public void testInjectionByConstructorExtended() {
		NeedItemByConstructorExtended needItem = DependancyInjector.injectByConstructor(NeedItemByConstructorExtended.class);
		
		assertNotNull(needItem);
		assertNotNull(needItem.item);
	}
	
	@Test
	public void testInjectionBySetterExtended() {
		NeedItemBySetterExtended needItem = DependancyInjector.injectBySetter(NeedItemBySetterExtended.class);
		
		assertNotNull(needItem);
		assertNotNull(needItem.item);
	}
	
	@Test
	public void testInjectionByFieldExtended() {
		NeedItemByFieldExtended needItem = DependancyInjector.injectByField(NeedItemByFieldExtended.class);
		
		assertNotNull(needItem);
		assertNotNull(needItem.item);
	}
	
}
