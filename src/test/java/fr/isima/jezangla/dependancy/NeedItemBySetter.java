package fr.isima.jezangla.dependancy;

import fr.isima.jezangla.dependancy.annotations.InjectBySetter;

public class NeedItemBySetter {
	public IItem item;
	
	@InjectBySetter
	public void setIItem(IItem item) {
		this.item = item;
	}
}