package fr.isima.jezangla.dependancy;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class DependancyContainerTest
{
    @Test
    public void usingProperRegistering() throws NoImplementationException
    {
    	DependancyContainer.register(IItem.class, Item.class);
    	assertEquals(Item.class.getName(), DependancyContainer.newInstance(IItem.class).getClass().getName());
    }
    
    @Test
    public void usingUnlogicRegistering()
    {
    	assertThrows(NoImplementationException.class, () -> {DependancyContainer.register(IItem.class, ArrayList.class);});
    }
    
    @Test
    public void noMappingButInstanciable()
    {
    	assertEquals(String.class.getName(), DependancyContainer.getClassForContract(String.class).getName());
    }
    
    @Test
    public void noMappingAndUninstanciable()
    {
    	assertNull(DependancyContainer.getClassForContract(Runnable.class));
    }
    
    @Test
    public void loadFromConfig()
    {
    	DependancyContainer.loadValues();
    	assertNotNull(DependancyContainer.getClassForContract(IItem.class));
    }
}
