package fr.isima.jezangla.dependancy;

import fr.isima.jezangla.dependancy.annotations.InjectByField;

public class NeedItemByField {
	
	@InjectByField
	public IItem item;
}