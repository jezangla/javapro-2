package fr.isima.jezangla.dependancy;

import fr.isima.jezangla.dependancy.annotations.InjectByConstructor;

public class NeedItemByConstructor {
	public IItem item;
	
	@InjectByConstructor
	public NeedItemByConstructor(IItem item) {
		this.item = item;
	}
}